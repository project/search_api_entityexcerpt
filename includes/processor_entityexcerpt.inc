<?php

/**
 * @file
 * Contains the SearchApiEntityExcerpt class.
 */

/**
 * Processor for highlighting search results.
 */
class SearchApiEntityExcerpt extends SearchApiHighlight {

  /**
   * PREG regular expression for a word boundary.
   *
   * We highlight around non-indexable or CJK characters.
   *
   * @var string
   */
  protected static $boundary;

  /**
   * PREG regular expression for splitting words.
   *
   * @var string
   */
  protected static $split;

  /**
   * {@inheritdoc}
   */
  public function __construct(SearchApiIndex $index, array $options = array()) {
    parent::__construct($index, $options);

    $cjk = '\x{1100}-\x{11FF}\x{3040}-\x{309F}\x{30A1}-\x{318E}' .
      '\x{31A0}-\x{31B7}\x{31F0}-\x{31FF}\x{3400}-\x{4DBF}\x{4E00}-\x{9FCF}' .
      '\x{A000}-\x{A48F}\x{A4D0}-\x{A4FD}\x{A960}-\x{A97F}\x{AC00}-\x{D7FF}' .
      '\x{F900}-\x{FAFF}\x{FF21}-\x{FF3A}\x{FF41}-\x{FF5A}\x{FF66}-\x{FFDC}' .
      '\x{20000}-\x{2FFFD}\x{30000}-\x{3FFFD}';
    self::$boundary = '(?:(?<=[' . PREG_CLASS_UNICODE_WORD_BOUNDARY . $cjk . '])|(?=[' . PREG_CLASS_UNICODE_WORD_BOUNDARY . $cjk . ']))';
    self::$split = '/[' . PREG_CLASS_UNICODE_WORD_BOUNDARY . ']+/iu';
  }

  /**
   * {@inheritdoc}
   */
  public function configurationForm() {
    $this->options += array(
      'prefix' => '<strong>',
      'suffix' => '</strong>',
      'excerpt' => TRUE,
      'excerpt_length' => 400,
      'highlight' => 'always',
      'mode' => 'default'
    );

    $form['prefix'] = array(
      '#type' => 'textfield',
      '#title' => t('Highlighting prefix'),
      '#description' => t('Text/HTML that will be prepended to all occurrences of search keywords in highlighted text.'),
      '#default_value' => $this->options['prefix'],
    );
    $form['suffix'] = array(
      '#type' => 'textfield',
      '#title' => t('Highlighting suffix'),
      '#description' => t('Text/HTML that will be appended to all occurrences of search keywords in highlighted text.'),
      '#default_value' => $this->options['suffix'],
    );


    $form['excerpt'] = array(
      '#type' => 'checkbox',
      '#title' => t('Create excerpt'),
      '#description' => t('When enabled, an excerpt will be created for searches with keywords, containing all occurrences of keywords in a fulltext field.'),
      '#default_value' => $this->options['excerpt'],
    );
    $form['excerpt_length'] = array(
      '#type' => 'textfield',
      '#title' => t('Excerpt length'),
      '#description' => t('The requested length of the excerpt, in characters.'),
      '#default_value' => $this->options['excerpt_length'],
      '#element_validate' => array('element_validate_integer_positive'),
      '#states' => array(
        'visible' => array(
          '#edit-processors-search-api-highlighting-settings-excerpt' => array(
            'checked' => TRUE,
          ),
        ),
      ),
    );

    $view_modes = array();
    if ($entity_type = $this->index->getEntityType()) {
      $info = entity_get_info($entity_type);
      foreach ($info['view modes'] as $key => $mode) {
        $view_modes[$key] = $mode['label'];
      }
    }
    $this->options += array('mode' => reset($view_modes));
    if (count($view_modes) > 1) {
      $form['mode'] = array(
        '#type' => 'select',
        '#title' => t('View mode'),
        '#description' => t('Entities are rendered using this view mode, and the the excerpt is created from the tag stripped HTML of this output.'),
        '#options' => $view_modes,
        '#default_value' => $this->options['mode'],
      );
    }
    else {
      $form['mode'] = array(
        '#type' => 'value',
        '#value' => $this->options['mode'],
      );
      if ($view_modes) {
        $form['note'] = array(
          '#markup' => '<p>' . t('Entities of type %type have only a single view mode. ' .
              'Therefore, no selection needs to be made.', array('%type' => $info['label'])) . '</p>',
        );
      }
      else {
        $form['note'] = array(
          '#markup' => '<p>' . t('Entities of type %type have no defined view modes. ' .
              'This might either mean that they are always displayed the same way, or that they cannot be processed by this alteration at all. ' .
              'Please consider this when using this alteration.', array('%type' => $info['label'])) . '</p>',
        );
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function postprocessSearchResults(array &$response, SearchApiQuery $query) {
    if (empty($response['results']) || !($keys = $this->getKeywords($query))) {
      return;
    }
    foreach ($response['results'] as $id => &$item) {
      if ($this->options['excerpt']) {
        $text = array();
        $fulltext_fields = $this->index->getFulltextFields();
        $fields = $this->getFulltextFields($response['results'], $id, $fulltext_fields);
        unset($fields['title']);
        foreach ($fields as $data) {
          if (is_array($data)) {
            $text = array_merge($text, $data);
          }
          else {
            $text[] = $data;
          }
        }

        // add the data from the search_data set (search_api_viewed)
        if (!empty($item['entity'])) {
          $this->renderEntity($item);
          if (!is_null($item['search_api_viewed'])) {
            $text[] = $item['search_api_viewed'];
          }
        }
        $item['excerpt'] = $this->createExcerpt($this->flattenArrayValues($text), $keys);
      }
    }
  }


  function renderEntity(&$item) {
    // Prevent session information from being saved while indexing.
    drupal_save_session(FALSE);

    // Force the current user to anonymous to prevent access bypass in search
    // indexes.
    $original_user = $GLOBALS['user'];
    $GLOBALS['user'] = drupal_anonymous_user();
    $mode = empty($this->options['mode']) ? 'full' : $this->options['mode'];
    $type = $this->index->getEntityType();
    try {
      $render = entity_view($type, array($item['entity']->type => $item['entity']), $mode);
      $text = render($render);
      $text = preg_replace("/<script.*?\\/script>/s", "", $text);
      if (!$text) {
        $item['search_api_viewed'] = NULL;
      }
      else {
        $item['search_api_viewed'] = preg_replace('/\s\s+/', ' ', trim(strip_tags($text)));
      }
    } catch (Exception $e) {
      $item['search_api_viewed'] = NULL;
    }
    $GLOBALS['user'] = $original_user;
    drupal_save_session(TRUE);
  }


  /**
   * Returns snippets from a piece of text, with certain keywords highlighted.
   *
   * Largely copied from search_excerpt().
   *
   * @param string $text
   *   The text to extract fragments from.
   * @param array $keys
   *   Search keywords entered by the user.
   *
   * @return string
   *   A string containing HTML for the excerpt.
   */
  protected function createExcerpt($text, array $keys) {
    // Prepare text by stripping HTML tags and decoding HTML entities.
    $text = strip_tags(str_replace(array('<', '>'), array(' <', '> '), $text));
    $text = ' ' . decode_entities($text);

    // Extract fragments around keywords.
    // First we collect ranges of text around each keyword, starting/ending
    // at spaces, trying to get to the requested length.
    // If the sum of all fragments is too short, we look for second occurrences.
    $ranges = array();
    $included = array();
    $length = 0;
    $workkeys = $keys;
    while ($length < $this->options['excerpt_length'] && count($workkeys)) {
      foreach ($workkeys as $k => $key) {
        if ($length >= $this->options['excerpt_length']) {
          break;
        }
        // Remember occurrence of key so we can skip over it if more occurrences
        // are desired.
        if (!isset($included[$key])) {
          $included[$key] = 0;
        }
        // Locate a keyword (position $p, always >0 because $text starts with a
        // space).
        $p = 0;
        if (preg_match('/' . self::$boundary . preg_quote($key, '/') . self::$boundary . '/iu', $text, $match, PREG_OFFSET_CAPTURE, $included[$key])) {
          $p = $match[0][1];
        }
        // Now locate a space in front (position $q) and behind it (position $s),
        // leaving about 60 characters extra before and after for context.
        // Note that a space was added to the front and end of $text above.
        if ($p) {
          if (($q = strpos(' ' . $text, ' ', max(0, $p - 61))) !== FALSE) {
            $end = substr($text . ' ', $p, 80);
            if (($s = strrpos($end, ' ')) !== FALSE) {
              // Account for the added spaces.
              $q = max($q - 1, 0);
              $s = min($s, strlen($end) - 1);
              $ranges[$q] = $p + $s;
              $length += $p + $s - $q;
              $included[$key] = $p + 1;
            }
            else {
              unset($workkeys[$k]);
            }
          }
          else {
            unset($workkeys[$k]);
          }
        }
        else {
          unset($workkeys[$k]);
        }
      }
    }

    if (count($ranges) == 0) {
      // We didn't find any keyword matches, so just return NULL.
      return NULL;
    }

    // Sort the text ranges by starting position.
    ksort($ranges);

    // Now we collapse overlapping text ranges into one. The sorting makes it O(n).
    $newranges = array();
    foreach ($ranges as $from2 => $to2) {
      if (!isset($from1)) {
        $from1 = $from2;
        $to1 = $to2;
        continue;
      }
      if ($from2 <= $to1) {
        $to1 = max($to1, $to2);
      }
      else {
        $newranges[$from1] = $to1;
        $from1 = $from2;
        $to1 = $to2;
      }
    }
    $newranges[$from1] = $to1;

    // Fetch text
    $out = array();
    foreach ($newranges as $from => $to) {
      $out[] = substr($text, $from, $to - $from);
    }

    // Let translators have the ... separator text as one chunk.
    $dots = explode('!excerpt', t('... !excerpt ... !excerpt ...'));

    $text = (isset($newranges[0]) ? '' : $dots[0]) . implode($dots[1], $out) . $dots[2];
    $text = check_plain($text);

    // Since we stripped the tags at the beginning, highlighting doesn't need to
    // handle HTML anymore.
    return $this->highlightField($text, $keys, FALSE);
  }
}
